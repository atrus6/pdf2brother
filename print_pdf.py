import sys
import tempfile
import time

import brother_ql
from brother_ql.raster import BrotherQLRaster
from brother_ql.backends.helpers import send
from brother_ql.labels import ALL_LABELS

import click

from pdf2image import convert_from_path

from PIL import Image

def pdf_to_images(filepath):
    images = convert_from_path(filepath)

    return images

def orient_images(images):
    rv = []

    for image in images:
        width, height = image.size
        if width > height:
            rv.append(image.transpose(Image.ROTATE_90))
        else:
            rv.append(image)

    return rv

def resize_images(images, size):
    rv = []

    #Find the size tuple
    size_tuple = None
    for label in ALL_LABELS:
        if label.identifier == size:
            size_tuple = label.dots_printable

    for image in images:
        rv.append(image.resize(size_tuple))
    return rv

def print_images(images, printer, size, cut):
    for image in images:
        print_data = brother_ql.brother_ql_create.convert(BrotherQLRaster('QL-1050'), [image], size, dither=True, cut=cut)
        send(print_data, printer)

def save_images(images):
    for num, image in enumerate(images):
        image.save('image-' + str(num) + '.png')

@click.command()
@click.option('--printer', default="usb://0x04f9:0x20a7", help='The USB ID of the printer')
@click.option('--size', default='102x152', help='The size (in mm) of the label to be printed.')
@click.option('--copies', default=1, help='The number of copies to print out.')
@click.option('--cut/--no-cut', help='Whether the printer cuts after each label.')
@click.argument('filename')
def entry(printer, size, copies, cut, filename):
    for x in range(copies):
        img = pdf_to_images(filename)
        img = orient_images(img)
        img = resize_images(img, size)
        try:
            if cut == False and (x+1) == copies:
                print_images(img, printer, size, True)
            else:
                print_images(img, printer, size, cut)
        except ValueError:
            print('Device not found. Is it turned on?')

if __name__ == '__main__':
    entry()

